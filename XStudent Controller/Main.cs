﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XStudent_Controller
{

    public partial class Main : Form
    {

        private string[] processNames = { "xstudent", "xclass" };
        private bool processesRunning = false;

        public Main()
        {
            InitializeComponent();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach (var process in Process.GetProcessesByName("XStudent Controller"))
            {
                process.Kill();
            }
        }

        private void Main_SizeChanged(object sender, EventArgs e)
        {
            bool MousePointerNotOnTaskBar = Screen.GetWorkingArea(this).Contains(Cursor.Position);

            if (this.WindowState == FormWindowState.Minimized && MousePointerNotOnTaskBar)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.Icon = SystemIcons.Application;
                notifyIcon1.BalloonTipText = "Hide...";
                notifyIcon1.ShowBalloonTip(1000);
                this.ShowInTaskbar = false;
            }
        }

        private void notifyIcon1_MouseDoubleClick_1(object sender, MouseEventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            if (this.WindowState == FormWindowState.Normal)
            {
                this.ShowInTaskbar = true;
                notifyIcon1.Visible = false;
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            HotKeyManager.RegisterHotKey(Keys.F1, KeyModifiers.None);
            HotKeyManager.RegisterHotKey(Keys.F2, KeyModifiers.None);
            HotKeyManager.HotKeyPressed += new EventHandler<HotKeyEventArgs>(HotKey_Pressed);
        }

        private void HotKey_Pressed(object sender, HotKeyEventArgs e)
        {
            if (e.Modifiers == KeyModifiers.None)
            {
                if (e.Key == Keys.F1)
                {
                    radioButton1.Checked = !radioButton1.Checked;
                }

                if (e.Key == Keys.F2)
                {
                    radioButton2.Checked = !radioButton2.Checked;
                }
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                timer1.Start();
                label1.Text = "Status: Stopped XStudent Client";
                label1.ForeColor = System.Drawing.Color.DodgerBlue;
            }

            else
            {
                timer1.Stop();
                label1.Text = "Status: Started XStudent Client";
                label1.ForeColor = System.Drawing.Color.Black;
            }
        }

        private void CheckAndHandleProcess(string processName, Timer timer)
        {
            Process[] processes = Process.GetProcessesByName(processName);

            if (processes.Length > 0)
            {
                foreach (var process in processes)
                {
                    process.Kill();
                }

                timer.Stop();
                MessageBox.Show($"{processName} processes have been terminated.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                try
                {
                    Process.Start(processName + ".exe");
                }
                catch (Exception ex)
                {
                    timer.Stop();
                    MessageBox.Show($"Error occurred while starting {processName}: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach (string processName in processNames)
            {
                CheckAndHandleProcess(processName, timer1);
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            foreach (string processName in processNames)
            {
                CheckAndHandleProcess(processName, timer2);
            }
        }


        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked == true)
            {
                timer2.Start();
                label1.Text = "Status: Started XStudent Client";
                label1.ForeColor = System.Drawing.Color.DodgerBlue;
            }

            else
            {
                timer2.Stop();
                label1.Text = "Status: Stopped XStudent Client";
                label1.ForeColor = System.Drawing.Color.DarkRed;
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are You Sure Close The Program?", "Close?", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private bool dragging = false;
        private Point startPoint = new Point(0, 0);

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            startPoint = new Point(e.X, e.Y);
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this.startPoint.X, p.Y - this.startPoint.Y);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
