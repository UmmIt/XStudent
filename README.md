## XStudent Controller

I don't like it when teachers control my computer when I'm in class. So I made this program to help me turn on and off XClass. This program is made when i was student.

This is just a simple script for stopping and starting XClass. You need to using UAC to use it.

Here is the program screenshots:

![sc1](./static/Program-Login.png)
![sc1](./static/Program-main.png)

## How to use?

To login the program: Username is `admin` and the password is `Admin`. After login, Use F1/F2 to toggle the ON/OFF Radio button.